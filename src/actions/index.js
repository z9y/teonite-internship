import teoniteAPI from '../apis/teoniteAPI'

export const selectedAuthor = selected => {
    return{
        type: 'SELECTED_AUTHOR',
        payload: selected
    }
};

export const selectedAuthorsList = selectedAuthor => {
    return {
        type: 'SELECTED_AUTHORS',
        payload: selectedAuthor
    }
};

export const deleteAuthor = deleteAuthor => {
    return{
        type: 'DELETE_AUTHOR',
        payload: deleteAuthor
    }
};

export const fetchAuthorsList = () => async dispatch => {
    const response = await teoniteAPI.get('/authors');

    dispatch({
        type: 'FETCH_AUTHORS',
        payload: response.data
    })
};

export const fetchAuthorsStats = selectedAuthors => async dispatch => {
    const response = await teoniteAPI.get(`/stats/${selectedAuthors}`);

    dispatch({
        type: 'FETCH_AUTHORS_STATS',
        payload: {
            data: response.data,
            authorName: selectedAuthors,
        },
    })
};

export const fetchAllStats = () => async dispatch => {
    const response = await teoniteAPI.get('/stats');

    dispatch({
        type: 'FETCH_ALL_STATS',
        payload: response.data
    })
};
