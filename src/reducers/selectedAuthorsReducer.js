export default (listOfAuthorsSelected = [], action) => {
    switch (action.type) {
        case 'SELECTED_AUTHORS':
            return [...listOfAuthorsSelected, action.payload];

        case 'SELECTED_AUTHOR':
            switch (action.payload) {
                case 'all':
                    return [];

                default:
                    return listOfAuthorsSelected;
            }

        case 'DELETE_AUTHOR':
            return listOfAuthorsSelected.filter(deleteAuthor => deleteAuthor !== action.payload);

        default:
            return listOfAuthorsSelected
    }
}