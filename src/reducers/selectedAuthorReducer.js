export default (selectedAuthor = 'all', action) => {
    switch (action.type) {
        case 'SELECTED_AUTHOR':
            return action.payload;

        default:
            return selectedAuthor
    }
}