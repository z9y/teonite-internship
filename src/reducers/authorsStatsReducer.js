export default (authorsData = {}, action) => {
    switch (action.type) {
        case 'FETCH_AUTHORS_STATS':
            return {
                ...authorsData,
                [action.payload.authorName]: action.payload.data,
            };

        case 'DELETE_AUTHOR' :
            const newState = {...authorsData};
            delete newState[action.payload];
            return newState;

        default:
            return authorsData
    }
}