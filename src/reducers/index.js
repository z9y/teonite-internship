import { combineReducers } from "redux";
import authorsFetchReducer from './authorsFetchReducer'
import selectedAuthorsReducer from './selectedAuthorsReducer'
import authorsStatsReducer from './authorsStatsReducer'
import allStatsReducer from './allStatsReducer'
import selectedAuthorReducer from './selectedAuthorReducer'

export default combineReducers({
    authorsList: authorsFetchReducer,
    selectedAuthorsList: selectedAuthorsReducer,
    authorsData: authorsStatsReducer,
    allStats: allStatsReducer,
    selectedAuthor: selectedAuthorReducer,
})