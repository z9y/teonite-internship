export default (allData = [], action) =>{
    switch (action.type) {
        case 'FETCH_ALL_STATS':
            return action.payload;

        default :
            return allData
    }
}