export default (state = [], action) => {
    switch (action.type) {
        case 'FETCH_AUTHORS':
            return action.payload;

        default:
            return state
    }
}