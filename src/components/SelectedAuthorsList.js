import React from 'react'
import { connect } from "react-redux";
import { fetchAuthorsList, deleteAuthor } from'../actions'

class SelectedAuthorsList extends React.Component{

    onClickDelete = key => {
        this.props.deleteAuthor(key)
    };

    render() {
        return (
            <div className='ui container'>
                {this.props.selectedAuthors.map((key) => {
                    return <div key={key}>{this.props.authorsList[key]}<button onClick={() => this.onClickDelete(key)} className="ui red mini button">X</button></div>
                })}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        selectedAuthors: state.selectedAuthorsList,
        authorsList: state.authorsList,
    }
};

export default connect(mapStateToProps, { fetchAuthorsList, deleteAuthor })(SelectedAuthorsList)