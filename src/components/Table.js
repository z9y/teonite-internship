import React from 'react'
import { connect } from "react-redux";
import { selectedAuthorsList } from '../actions'

class Table extends React.Component {

    table = () => {
        const addValue = Object.values(this.props.authorsData).reduce((a, b) => {
                        for (let k in b) {
                            if (b.hasOwnProperty(k))
                                a[k] = (a[k] || 0) + b[k];
                        }
                        return a;
                    }, {});

        const renderValue = this.props.selectedAuthors.length === 0 ? this.props.allStats : addValue;

                    const words = Object.keys(renderValue).map(key => {
                        return {word: key, count: renderValue[key]}
                    });

                    words.sort((obj1, obj2) => {
                        return obj2.count - obj1.count
                    });
                    return (
                        <table className='ui celled table'>
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Word</th>
                                <th>Count</th>
                            </tr>
                            </thead>
                            <tbody>
                            {words.map((key, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{key.word}</td>
                                        <td>{key.count}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    )
    };

    render() {

        return (<div>{this.table()}</div>)

    }
}

const mapStateToProps = state => {
    return {
        selectedAuthors: state.selectedAuthorsList,
        authorsList: state.authorsList,
        authorsData: state.authorsData,
        allStats: state.allStats,
        selectedAuthor: state.selectedAuthor
    }
};

export default connect(mapStateToProps, { selectedAuthorsList })(Table)
