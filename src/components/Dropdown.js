import React from 'react'
import { connect } from "react-redux";
import { fetchAuthorsList, selectedAuthorsList, fetchAuthorsStats, fetchAllStats, selectedAuthor } from'../actions'

class Dropdown extends React.Component{

    componentDidMount() {
        this.props.fetchAuthorsList();
        this.props.fetchAllStats();
        this.props.selectedAuthors.map(data => {
            return this.props.fetchAuthorsStats(data)
        })

    }

    onChangeSelect = e => {
        const value = e.target.value;
        this.props.selectedAuthor(value);
        if (value !== 'all') {
            if (this.props.selectedAuthors.includes(value)){
            } else {
                this.props.selectedAuthorsList(value);
                this.props.fetchAuthorsStats(value)
            }
        }
    };

    render() {
        return (
            <select onChange={this.onChangeSelect} className='ui fluid search dropdown'>
                <option value='all' defaultChecked={true}>All</option>
                {Object.keys(this.props.authorsList).map((key, index) => {
                    return (
                        <option key={index} value={key}>{this.props.authorsList[key]}</option>
                    )
                })}
            </select>
        );
    }
}

const mapStateToProps = state => {
    return {
        authorsList: state.authorsList,
        selectedAuthors: state.selectedAuthorsList,
        authorsData: state.authorsData,
        selectedAuthor: state.selectedAuthor
    }
};

export default connect(mapStateToProps, { fetchAuthorsList, selectedAuthorsList, fetchAuthorsStats, fetchAllStats, selectedAuthor })(Dropdown)