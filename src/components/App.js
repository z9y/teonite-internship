import React from 'react'
import Dropdown from './Dropdown'
import SelectedAuthorsList from './SelectedAuthorsList'
import Table from'./Table'
import { BrowserRouter, Route} from "react-router-dom";

const App = () => {
        return (
            <div>
                <BrowserRouter>
                    <div className='ui container'>
                        <Route path='/stats' exact component={Dropdown}/>
                        <Route path='/stats' exact component={SelectedAuthorsList}/>
                        <Route path='/stats' exact component={Table}/>
                    </div>
                </BrowserRouter>
            </div>
        );
};

export default App
